package ru.cyber_eagle_owl.climaticsouls.model.blogic;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import java.util.LinkedList;
import java.util.List;

import ru.cyber_eagle_owl.climaticsouls.loaders.ForecastDeleteLoader;
import ru.cyber_eagle_owl.climaticsouls.db.provider.ForecastContract;
import ru.cyber_eagle_owl.climaticsouls.ui.screens.base.BaseActivity;
import ru.cyber_eagle_owl.climaticsouls.ui.screens.base.MvpView;
import ru.cyber_eagle_owl.climaticsouls.ui.screens.main.MainMvpView;

public class BusinessObject implements BusinessLogic {

    private static final String TAG = "Weather/BusinessObject";

    private static final long TIME_DELAY = 86400000; //86400000;

    private MvpView mvpView;

    private Location lastKnownLocation;

    @Override
    public void setMvpView(final MvpView mvpView) {
        this.mvpView = mvpView;
    }

    @Override
    public boolean isStorageTimeForThisForecastExpired(final long forecastData) {
        if (forecastData > System.currentTimeMillis() - TIME_DELAY) {// 86400000
            //if (forecastData > System.currentTimeMillis()) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public Bundle prepareSelectionForOld5DayForecastDeleting(final LinkedList<Long> listOfOld5DayForecastsIds) {
// TODO: 02.09.2018 доделать удаление
        Bundle deletingLoaderArgs = new Bundle();

        String selectionForDeletingOldForecasts;
        String[] selectionArgsForDeletingOldForecasts = new String[listOfOld5DayForecastsIds.size()];

        if (listOfOld5DayForecastsIds.size() > 1) {

            StringBuilder sbSelectionForDeleting = new StringBuilder(ForecastContract.Forecasts._ID + " IN (");
            String tmpSeparator = ", ";

            int tmpCount = 0;

            for (Long id : listOfOld5DayForecastsIds) {
                sbSelectionForDeleting.append("?");
                sbSelectionForDeleting.append(tmpSeparator);

                selectionArgsForDeletingOldForecasts[tmpCount++] = id.toString();
            }

            sbSelectionForDeleting.delete(
                    sbSelectionForDeleting.lastIndexOf(tmpSeparator),
                    sbSelectionForDeleting.length()
            );

            sbSelectionForDeleting.append(")");
            selectionForDeletingOldForecasts = String.valueOf(sbSelectionForDeleting);

            Log.i(TAG, "Получились такие selectionForDeletingOldForecasts: " + selectionForDeletingOldForecasts);
            Log.i(TAG, "Получились такие selectionArgsForDeletingOldForecasts: " + String.valueOf(selectionArgsForDeletingOldForecasts));

        } else {
            selectionForDeletingOldForecasts = ForecastContract.Forecasts._ID + " = ?";
            Log.i(TAG, "Получились такие selectionForDeletingOldForecasts: " + selectionForDeletingOldForecasts);
            selectionArgsForDeletingOldForecasts[0] = String.valueOf(listOfOld5DayForecastsIds.getFirst());
            Log.i(TAG, "Получились такие selectionArgsForDeletingOldForecasts: " + String.valueOf(selectionArgsForDeletingOldForecasts[0]));
        }

        deletingLoaderArgs.putString(ForecastDeleteLoader.KEY_SELECTION_FOR_OLD_FORECAST_DELETING, selectionForDeletingOldForecasts);
        deletingLoaderArgs.putStringArray(ForecastDeleteLoader.KEY_SELECTION_ARGS_FOR_OLD_FORECAST_DELETING, selectionArgsForDeletingOldForecasts);

        return deletingLoaderArgs;
    }


    @Override
    public String getBestProvider() {
        // Получаем LocationManager
        LocationManager locationManager = ((MainMvpView) mvpView).getLocationManagerFromMainMvpView();

        // Получаем лучший провайдер
        Criteria criteria = new Criteria();

        String bestProvider = locationManager.getBestProvider(criteria, true);

        Log.v(TAG, "Best provider: " + bestProvider);
        return bestProvider;
    }

    @Override
    public Double getLatitudeFromLastKnownLocation() {
        if (lastKnownLocation != null) {
            return lastKnownLocation.getLatitude();
        } else {
            return null;
        }
    }

    @Override
    public Double getLongitudeFromLastKnownLocation() {
        if (lastKnownLocation != null) {
            return lastKnownLocation.getLongitude();
        } else {
            return null;
        }
    }

    @Override
    public Location getLastKnownLocation(String bestProvider) {
        LocationManager locationManager = ((MainMvpView) mvpView).getLocationManagerFromMainMvpView();

        if (bestProvider != null) {

            // На всякий случай проверим, не убрал ли пользователь разрешение на ГЕО
            if (ActivityCompat.checkSelfPermission(((BaseActivity) mvpView),
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED

                    && ActivityCompat.checkSelfPermission(((BaseActivity) mvpView),
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return null;
            }

            List<String> providers = locationManager.getProviders(true);
            for (String provider : providers) {
                Log.i(TAG, "Provider: " + provider);
            }
            lastKnownLocation = locationManager.getLastKnownLocation(bestProvider);
            // Возвращаем последнюю доступную позицию
            Log.i(TAG, "Возвращаем lastKnownLocation = " + lastKnownLocation);
            return lastKnownLocation;
        }
        return null;
    }
}