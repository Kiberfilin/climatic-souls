package ru.cyber_eagle_owl.climaticsouls.model.preferences;

import android.content.SharedPreferences;
import android.support.annotation.Nullable;

public class SharedPreferencesHelper {
    private SharedPreferences mSharedPreferences;

    public SharedPreferencesHelper(SharedPreferences mSharedPreferences) {
            this.mSharedPreferences = mSharedPreferences;
        }

@Nullable
    public Long getLastUpdate() {
        if (mSharedPreferences != null) {
            return mSharedPreferences.getLong("last_update", 0);
        }
        return null;
    }
}
