
package ru.cyber_eagle_owl.climaticsouls.model.pojo.forecast5day;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class FiveDayForecast implements Parcelable{

    @SerializedName("cnt")
    @Expose
    private Integer mMeasurementsCount;
    @SerializedName("list")
    @Expose
    private java.util.List<Measurements> mMeasurementsList = null;

    /**
     * No args constructor for use in serialization
     * 
     */
    public FiveDayForecast() {
    }

    /**
     *
     * @param mMeasurementsCount Number of measurementsList returned by API call
     * @param mMeasurementsList List of measurementsList returned by API call
     */
    public FiveDayForecast(Integer mMeasurementsCount, java.util.List<Measurements> mMeasurementsList) {
        super();
        this.mMeasurementsCount = mMeasurementsCount;
        this.mMeasurementsList = mMeasurementsList;
    }

    protected FiveDayForecast(Parcel in) {
        if (in.readByte() == 0) {
            mMeasurementsCount = null;
        } else {
            mMeasurementsCount = in.readInt();
        }
        mMeasurementsList = in.createTypedArrayList(Measurements.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (mMeasurementsCount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(mMeasurementsCount);
        }
        dest.writeTypedList(mMeasurementsList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FiveDayForecast> CREATOR = new Creator<FiveDayForecast>() {
        @Override
        public FiveDayForecast createFromParcel(Parcel in) {
            return new FiveDayForecast(in);
        }

        @Override
        public FiveDayForecast[] newArray(int size) {
            return new FiveDayForecast[size];
        }
    };

    public Integer getMeasurementsCount() {
        return mMeasurementsCount;
    }

    public void setMeasurementsCount(Integer measurementsCount) {
        this.mMeasurementsCount = measurementsCount;
    }

    public java.util.List<Measurements> getMeasurementsList() {
        return mMeasurementsList;
    }

    public void setMeasurementsList(java.util.List<Measurements> measurementsList) {
        this.mMeasurementsList = measurementsList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("mMeasurementsCount", mMeasurementsCount).append("mMeasurementsList", mMeasurementsList).toString();
    }

}
