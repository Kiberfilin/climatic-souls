
package ru.cyber_eagle_owl.climaticsouls.model.pojo.forecast5day;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Clouds implements Parcelable {

    @SerializedName("all")
    @Expose
    private Integer cloudsPercentage;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Clouds() {
    }

    /**
     * 
     * @param cloudsPercentage Cloudiness, %
     */
    public Clouds(Integer cloudsPercentage) {
        super();
        this.cloudsPercentage = cloudsPercentage;
    }

    protected Clouds(Parcel in) {
        if (in.readByte() == 0) {
            cloudsPercentage = null;
        } else {
            cloudsPercentage = in.readInt();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (cloudsPercentage == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(cloudsPercentage);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Clouds> CREATOR = new Creator<Clouds>() {
        @Override
        public Clouds createFromParcel(Parcel in) {
            return new Clouds(in);
        }

        @Override
        public Clouds[] newArray(int size) {
            return new Clouds[size];
        }
    };

    public Integer getCloudsPercentage() {
        return cloudsPercentage;
    }

    public void setCloudsPercentage(Integer cloudsPercentage) {
        this.cloudsPercentage = cloudsPercentage;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("cloudsPercentage", cloudsPercentage).toString();
    }
}
