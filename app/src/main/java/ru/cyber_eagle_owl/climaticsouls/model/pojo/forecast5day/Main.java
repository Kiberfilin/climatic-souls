
package ru.cyber_eagle_owl.climaticsouls.model.pojo.forecast5day;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Main implements Parcelable {

    @SerializedName("temp")
    @Expose
    private Double temp;
    @SerializedName("temp_min")
    @Expose
    private Double tempMin;
    @SerializedName("temp_max")
    @Expose
    private Double tempMax;
    @SerializedName("pressure")
    @Expose
    private Double pressure;
    @SerializedName("sea_level")
    @Expose
    private Double seaLevelPressure;
    @SerializedName("grnd_level")
    @Expose
    private Double grndLevelPressure;
    @SerializedName("humidity")
    @Expose
    private Integer humidityPercentage;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Main() {
    }

    /**
     *
     * @param temp Temperature
     * @param tempMin Minimum temperature at the moment of calculation. This is deviation from 'temp' that is possible for large cities and megalopolises geographically expanded (use these parameter optionally)
     * @param tempMax Maximum temperature at the moment of calculation. This is deviation from 'temp' that is possible for large cities and megalopolises geographically expanded (use these parameter optionally)
     * @param pressure Atmospheric pressure on the sea level by default, hPa
     * @param seaLevelPressure Atmospheric pressure on the sea level, hPa
     * @param grndLevelPressure Atmospheric pressure on the ground level, hPa
     * @param humidityPercentage Humidity, %
     */
    public Main(Double temp, Double tempMin, Double tempMax, Double pressure, Double seaLevelPressure, Double grndLevelPressure, Integer humidityPercentage) {
        super();
        this.temp = temp;
        this.tempMin = tempMin;
        this.tempMax = tempMax;
        this.pressure = pressure;
        this.seaLevelPressure = seaLevelPressure;
        this.grndLevelPressure = grndLevelPressure;
        this.humidityPercentage = humidityPercentage;
    }

    protected Main(Parcel in) {
        if (in.readByte() == 0) {
            temp = null;
        } else {
            temp = in.readDouble();
        }
        if (in.readByte() == 0) {
            tempMin = null;
        } else {
            tempMin = in.readDouble();
        }
        if (in.readByte() == 0) {
            tempMax = null;
        } else {
            tempMax = in.readDouble();
        }
        if (in.readByte() == 0) {
            pressure = null;
        } else {
            pressure = in.readDouble();
        }
        if (in.readByte() == 0) {
            seaLevelPressure = null;
        } else {
            seaLevelPressure = in.readDouble();
        }
        if (in.readByte() == 0) {
            grndLevelPressure = null;
        } else {
            grndLevelPressure = in.readDouble();
        }
        if (in.readByte() == 0) {
            humidityPercentage = null;
        } else {
            humidityPercentage = in.readInt();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (temp == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(temp);
        }
        if (tempMin == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(tempMin);
        }
        if (tempMax == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(tempMax);
        }
        if (pressure == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(pressure);
        }
        if (seaLevelPressure == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(seaLevelPressure);
        }
        if (grndLevelPressure == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(grndLevelPressure);
        }
        if (humidityPercentage == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(humidityPercentage);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Main> CREATOR = new Creator<Main>() {
        @Override
        public Main createFromParcel(Parcel in) {
            return new Main(in);
        }

        @Override
        public Main[] newArray(int size) {
            return new Main[size];
        }
    };

    public Double getTemp() {
        return temp;
    }

    public void setTemp(Double temp) {
        this.temp = temp;
    }

    public Double getTempMin() {
        return tempMin;
    }

    public void setTempMin(Double tempMin) {
        this.tempMin = tempMin;
    }

    public Double getTempMax() {
        return tempMax;
    }

    public void setTempMax(Double tempMax) {
        this.tempMax = tempMax;
    }

    public Double getPressure() {
        return pressure;
    }

    public void setPressure(Double pressure) {
        this.pressure = pressure;
    }

    public Double getSeaLevelPressure() {
        return seaLevelPressure;
    }

    public void setSeaLevelPressure(Double seaLevelPressure) {
        this.seaLevelPressure = seaLevelPressure;
    }

    public Double getGrndLevelPressure() {
        return grndLevelPressure;
    }

    public void setGrndLevelPressure(Double grndLevelPressure) {
        this.grndLevelPressure = grndLevelPressure;
    }

    public Integer getHumidityPercentage() {
        return humidityPercentage;
    }

    public void setHumidityPercentage(Integer humidityPercentage) {
        this.humidityPercentage = humidityPercentage;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("temp", temp).append("tempMin", tempMin).append("tempMax", tempMax).append("pressure", pressure).append("seaLevelPressure", seaLevelPressure).append("grndLevelPressure", grndLevelPressure).append("humidityPercentage", humidityPercentage).toString();
    }
}
