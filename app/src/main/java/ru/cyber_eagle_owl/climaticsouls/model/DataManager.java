package ru.cyber_eagle_owl.climaticsouls.model;

import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;

import java.util.LinkedList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.cyber_eagle_owl.climaticsouls.dagger.annotations.ApplicationContext;
import ru.cyber_eagle_owl.climaticsouls.loaders.ForecastDeleteLoader;
import ru.cyber_eagle_owl.climaticsouls.loaders.ForecastSavingLoader;
import ru.cyber_eagle_owl.climaticsouls.model.api.ApiHelper;
import ru.cyber_eagle_owl.climaticsouls.model.blogic.BusinessLogic;
import ru.cyber_eagle_owl.climaticsouls.model.blogic.BusinessObject;
import ru.cyber_eagle_owl.climaticsouls.db.DbHelper;
import ru.cyber_eagle_owl.climaticsouls.db.provider.ForecastContract;
import ru.cyber_eagle_owl.climaticsouls.model.pojo.forecast5day.FiveDayForecast;
import ru.cyber_eagle_owl.climaticsouls.model.preferences.SharedPreferencesHelper;
import ru.cyber_eagle_owl.climaticsouls.ui.screens.main.MainMvpView;

import static ru.cyber_eagle_owl.climaticsouls.loaders.ForecastSavingLoader.KEY_CONTENT_VALUES;
import static ru.cyber_eagle_owl.climaticsouls.loaders.ForecastSavingLoader.KEY_URI;

public class DataManager implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final String TAG = "Weather/DataManager";
    private static final String TAG2 = "0==(========>";

    private ApiHelper apiHelper;
    private SharedPreferencesHelper mPreferencesHelper;
    private DbHelper mDbHelper;
    private Context starterApp;
    private MainMvpView mainMvpView;
    private BusinessLogic businessLogicObject;

    @Nullable
    private OnFiveDayForecastReadyListener onFiveDayForecastReadyListener;

    private AsyncTaskLoaderCallback callback = new AsyncTaskLoaderCallback();

    private static final int PREPARE_DB_CURSOR_LOADER_ID = 0;
    private static final int PREPARE_DB_DELETING_LOADER_ID = 1;
    private static final int PREPARE_DB_SAVING_LOADER_ID = 2;
    private static final int PREPARE_DB_FIVE_DAY_FORECAST_LOADER_ID = 3;

    private static final String TAG_FOR_PREPARE_DB_SELECTION = "tmpSelection";
    private static final String TAG_FOR_PREPARE_DB_SELECTION_ARGS = "tmpSelectionArgs";

    public DataManager(SharedPreferences mSharedPreferences, @ApplicationContext Context starterApp, ApiHelper apiHelper) {
        this.apiHelper = apiHelper;
        this.mPreferencesHelper = new SharedPreferencesHelper(mSharedPreferences);
        this.mDbHelper = new DbHelper(starterApp);
        this.starterApp = starterApp;
        this.businessLogicObject = new BusinessObject();
    }

    public void setMainMvpView(final MainMvpView mainMvpView) {
        Log.i(TAG2, "Находимся в " + TAG + " в методе setMainMvpView, сейчас " +
                "будем устанавливать MvpView: this.mainMvpView = mainMvpView;");
        this.mainMvpView = mainMvpView;
        businessLogicObject.setMvpView(mainMvpView);
    }

    public void setOnFiveDayForecastReadyListener(final DataManager.OnFiveDayForecastReadyListener onFiveDayForecastReadyListener) {
        Log.i(TAG2, "Находимся в " + TAG + " в методе setOnFiveDayForecastReadyListener, сейчас " +
                "будем устанавливать слушатель: this.onFiveDayForecastReadyListener = onFiveDayForecastReadyListener;");
        this.onFiveDayForecastReadyListener = onFiveDayForecastReadyListener;
    }

    public Long getLastUpdate() {
        return mPreferencesHelper.getLastUpdate();
    }

    public void prepareDb() {
        String tmpSelection = ForecastContract.Forecasts.COLUMN_TYPE_OF_FORECAST + " = ?";
        String[] tmpSelectionArgs = {ForecastContract.Forecasts.TYPE_OF_FORECAST_5DAY_FORECAST};

        Bundle args = new Bundle();
        args.putString(TAG_FOR_PREPARE_DB_SELECTION, tmpSelection);
        args.putStringArray(TAG_FOR_PREPARE_DB_SELECTION_ARGS, tmpSelectionArgs);

        if (mainMvpView != null) {
            Log.i(TAG2, "Находимся в " + TAG + " в методе prepareDb(), сейчас " +
                    "будем запускать лоадер: mainMvpView.getLoaderManagerFromMainMvpView().initLoader(PREPARE_DB_CURSOR_LOADER_ID, args, this);");
            mainMvpView.getLoaderManagerFromMainMvpView().initLoader(PREPARE_DB_CURSOR_LOADER_ID, args, this);
        } else {
            Log.i(TAG, "mainMvpView = " + mainMvpView + "!!!");
        }
    }

    private CursorLoader prepareDbCursorLoader(final Bundle args) {
        Log.i(TAG2, "Находимся в " + TAG + " в методе onCreateLoader, сейчас " +
                "будем создавать лоадер: return new CursorLoader(starterApp,\n" +
                "                        ForecastContract.Forecasts.URI,\n" +
                "                        ForecastContract.Forecasts.projectionForPrepareDb,\n" +
                "                        args.getString(\"tmpSelection\"),\n" +
                "                        args.getStringArray(\"tmpSelectionArgs\"),\n" +
                "                        null);");

        return new CursorLoader(starterApp,
                ForecastContract.Forecasts.URI,
                ForecastContract.Forecasts.projectionForPrepareDb,
                args.getString("tmpSelection"),
                args.getStringArray("tmpSelectionArgs"),
                null);
    }

    private CursorLoader prepareDbFiveDayForecastLoader(final Bundle args) {
        Log.i(TAG2, "Находимся в " + TAG + " в методе onCreateLoader, сейчас " +
                "будем создавать лоадер: return new CursorLoader(starterApp,\n" +
                "                        ForecastContract.Forecasts.URI,\n" +
                "                        ForecastContract.Forecasts.projectionForPrepareDbFiveDayForecastLoader,\n" +
                "                        null, null,\n" +
                "                        ForecastContract.Forecasts.COLUMN_DATE_OF_FORECAST + \" DESC LIMIT 1\");");
        return new CursorLoader(starterApp,
                ForecastContract.Forecasts.URI,
                ForecastContract.Forecasts.projectionForPrepareDbFiveDayForecastLoader,
                null, null,
                ForecastContract.Forecasts.COLUMN_DATE_OF_FORECAST + " DESC LIMIT 1");
    }

    @Override
    public Loader<Cursor> onCreateLoader(final int id, final Bundle args) {
        switch (id) {
            case PREPARE_DB_CURSOR_LOADER_ID:
                return prepareDbCursorLoader(args);

            case PREPARE_DB_FIVE_DAY_FORECAST_LOADER_ID:
                return prepareDbFiveDayForecastLoader(args);

            default:
                return null;
        }
    }

    private void deletingOld5DayForecasts(final LinkedList<Long> listOfOld5DayForecastsIds) {
        Log.i(TAG, "Запускаем " + ForecastDeleteLoader.class.getSimpleName());
        mainMvpView.getLoaderManagerFromMainMvpView().initLoader(PREPARE_DB_DELETING_LOADER_ID,
                businessLogicObject.prepareSelectionForOld5DayForecastDeleting(listOfOld5DayForecastsIds),
                callback);
    }

    private void startFiveDayForecastLoader() {
        if (mainMvpView != null) {
            Log.i(TAG2, "Находимся в " + TAG + " в методе startFiveDayForecastLoader(), имея: " +
                    "mainMvpView != null, видя это, мы запускаем лоадер: " +
                    "mainMvpView.getLoaderManagerFromMainMvpView().getLoaderManager().initLoader(PREPARE_DB_FIVE_DAY_FORECAST_LOADER_ID, null, this);");
            mainMvpView.getLoaderManagerFromMainMvpView().initLoader(PREPARE_DB_FIVE_DAY_FORECAST_LOADER_ID, null, this);
        } else {
            Log.i(TAG2, "Находимся в " + TAG + " в методе startFiveDayForecastLoader(), не " +
                    "получилось запустить FiveDayForecastLoader потому что mainMvpView = " + mainMvpView);
        }
    }

    public void insert(final String typeOfForecast, final String forecastJson) {
        Log.i(TAG, "Сейчас готовится к запуску " + ForecastSavingLoader.class.getSimpleName() +
                " чтобы сохранить в бд " + typeOfForecast +": " + forecastJson);
        ContentValues contentValues = new ContentValues();
        contentValues.put(ForecastContract.Forecasts.COLUMN_TYPE_OF_FORECAST, typeOfForecast);
        contentValues.put(ForecastContract.Forecasts.COLUMN_DATE_OF_FORECAST, System.currentTimeMillis());
        contentValues.put(ForecastContract.Forecasts.COLUMN_FORECAST_JSON, forecastJson);

        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_CONTENT_VALUES, contentValues);
        bundle.putParcelable(KEY_URI, ForecastContract.Forecasts.URI);

        if (mainMvpView != null) {
            mainMvpView.getLoaderManagerFromMainMvpView().initLoader(PREPARE_DB_SAVING_LOADER_ID, bundle, callback);
        } else {
            Log.i(TAG, "mainMvpView = " + mainMvpView);
        }
    }

    private void getNewest5DayForecast() {
        final Gson gson = new Gson();
        String bestProvider = businessLogicObject.getBestProvider();
        if (bestProvider != null) {
            if (businessLogicObject.getLastKnownLocation(bestProvider) != null) {
                double tmpLatitude = businessLogicObject.getLatitudeFromLastKnownLocation();
                double tmpLongitude = businessLogicObject.getLongitudeFromLastKnownLocation();
                Log.i(TAG2, "tmpLatitude = " + tmpLatitude + ", tmpLongitude = " + tmpLongitude);
                apiHelper.getCallFor5DayForecast(tmpLatitude, tmpLongitude)
                        .enqueue(new Callback<FiveDayForecast>() {
                            @Override
                            public void onResponse(@NonNull final Call<FiveDayForecast> call, @NonNull final Response<FiveDayForecast> response) {
                                if (response.isSuccessful()) {
                                    String fiveDayForecastJson = gson.toJson(response.body());
                                    Log.i(TAG, "Подгрузили " + ForecastContract.Forecasts.TYPE_OF_FORECAST_5DAY_FORECAST + ": " + fiveDayForecastJson);
                                    Log.i(TAG, "Теперь делаем insert в базу данных.");
                                    insert(ForecastContract.Forecasts.TYPE_OF_FORECAST_5DAY_FORECAST, fiveDayForecastJson);
                                }
                            }

                            @Override
                            public void onFailure(@NonNull final Call<FiveDayForecast> call, @NonNull final Throwable t) {
                                Log.i(TAG, "Не удалось загрузить " + ForecastContract.Forecasts.TYPE_OF_FORECAST_5DAY_FORECAST);
                                Log.i(TAG, t.getMessage());
                            }
                        });
            } else {
                mainMvpView.showToastMsg("Unfortunately your location is unknown yet... Please try again later.");
                Log.i(TAG2, "businessLogicObject.getLastKnownLocation(bestProvider) = " + businessLogicObject.getLastKnownLocation(bestProvider));
            }
        } else {
            Log.i(TAG2, "bestProvider = " + bestProvider);
            mainMvpView.showToastMsg("Unfortunately your location is unknown yet... Please try again later.");
        }
    }

    private void onPrepareDbCursorLoaderLoadFinished(Cursor cursor) {
        int cursorEntryCount; // Количество строк в курсоре

        // Валидны ли данные
        // Данные корректны если курсор не null
        boolean isDataValid = cursor != null;

        // Индекс столбца ID в курсоре
        // Пытаемся получить индекс столбца ID, если курсор не null, в ином случае -1
        int idColumnIndex = cursor != null
                ? cursor.getColumnIndexOrThrow(ForecastContract.Forecasts._ID)
                : -1;

        // Индекс столбца type_of_forecast в курсоре
        int typeOfForecastColumnIndex = cursor != null
                ? cursor.getColumnIndexOrThrow(ForecastContract.Forecasts.COLUMN_TYPE_OF_FORECAST)
                : -1;

         // Индекс столбца date_of_forecast в курсоре
        int dateOfForecastColumnIndex = cursor != null
                ? cursor.getColumnIndexOrThrow(ForecastContract.Forecasts.COLUMN_DATE_OF_FORECAST)
                : -1;

        if (isDataValid) {
            cursorEntryCount = cursor.getCount();
            Log.i(TAG, "Строк в курсоре: " + cursorEntryCount);

            int total5DayForecastsCount = 0;
            LinkedList<Long> listOfOld5DayForecastsIds = new LinkedList<>();

            if (cursorEntryCount > 0) {
                while (cursor.moveToNext()) {
                    switch (cursor.getString(typeOfForecastColumnIndex)) {
                        case ForecastContract.Forecasts.TYPE_OF_FORECAST_5DAY_FORECAST:
                            total5DayForecastsCount++;
                            if (businessLogicObject.isStorageTimeForThisForecastExpired(cursor
                                    .getLong(dateOfForecastColumnIndex))) {
                                // собираем id просроченных прогнозов, чтобы потом удалить
                                listOfOld5DayForecastsIds.add(cursor.getLong(idColumnIndex)); }
                            break;
                    }
                }

                Log.i(TAG, "Всего пятидневных прогнозов: " + total5DayForecastsCount + ", из них просрочено: " + listOfOld5DayForecastsIds.size());

                if (listOfOld5DayForecastsIds.size() > 0) {
                    // в случае если нашли хотя бы один старый пятидневный прогноз: удаляем все найденные старые пятидневные прогнозы
                    deletingOld5DayForecasts(listOfOld5DayForecastsIds);
                    // запускаем лоадер для получения не просроченного прогноза из базы
                    Log.i(TAG2, "Находимся в " + TAG + " в методе onPrepareDbCursorLoaderLoadFinished, сейчас " +
                            "запускаем лоадер для получения не просроченного прогноза из базы, в случае, если onFiveDayForecastReadyListener != null");
                    if (onFiveDayForecastReadyListener != null) {
                        Log.i(TAG2, "Находимся в " + TAG + " в методе onPrepareDbCursorLoaderLoadFinished, сейчас " +
                                "видим, что onFiveDayForecastReadyListener != null, поэтому делаем startFiveDayForecastLoader();");
                        startFiveDayForecastLoader();
                    }
                } else if (total5DayForecastsCount == listOfOld5DayForecastsIds.size()) {
                    Log.i(TAG, "Удалили из бд все пятидневные прогнозы, значит надо загрузить свежий прогноз");
                    getNewest5DayForecast();
                } else if (listOfOld5DayForecastsIds.size() == 0 && total5DayForecastsCount >= 1){
                    if (onFiveDayForecastReadyListener != null) {
                        Log.i(TAG2, "Находимся в " + TAG + " в методе onPrepareDbCursorLoaderLoadFinished, сейчас " +
                                "видим, что onFiveDayForecastReadyListener != null и выполняется условие: " +
                                "listOfOld5DayForecastsIds.size() == 0 && total5DatForecastsCount >= 1, " +
                                "поэтому делаем startFiveDayForecastLoader();");
                        startFiveDayForecastLoader();
                    }
                }
            } else {
                Log.i(TAG, "нет пятидневных прогнозов в бд, значит надо загрузить свежий прогноз");
                getNewest5DayForecast();
            }
        }
    }

    private void onPrepareDbFiveDayForecastCursorLoaderLoadFinished(final Cursor cursor) {
        boolean isDataValid; // Валидны ли данные
        int idColumnIndex; // Индекс столбца ID в курсоре
        int typeOfForecastColumnIndex; // Индекс столбца type_of_forecast в курсоре
        int dateOfForecastColumnIndex; // Индекс столбца date_of_forecast в курсоре
        int forecastJsonColumnIndex; // Индекс столбца forecast_json в курсоре
        String tmpForecastJson;

        // Данные корректны если курсор не null
        isDataValid = cursor != null;

        // Пытаемся получить индекс столбца ID, если курсор не null, в ином случае -1
        idColumnIndex = cursor != null
                ? cursor.getColumnIndexOrThrow(ForecastContract.Forecasts._ID)
                : -1;

        typeOfForecastColumnIndex = cursor != null
                ? cursor.getColumnIndexOrThrow(ForecastContract.Forecasts.COLUMN_TYPE_OF_FORECAST)
                : -1;

        dateOfForecastColumnIndex = cursor != null
                ? cursor.getColumnIndexOrThrow(ForecastContract.Forecasts.COLUMN_DATE_OF_FORECAST)
                : -1;

        forecastJsonColumnIndex = cursor != null
                ? cursor.getColumnIndexOrThrow(ForecastContract.Forecasts.COLUMN_FORECAST_JSON)
                : -1;

        if (isDataValid) {
            Log.i(TAG2, "Находясь в " + TAG + " в методе onPrepareDbFiveDayForecastCursorLoaderLoadFinished видим, что "
                    + "isDataValid = " + isDataValid + " поэтому вынимаем из cursor строчку json с пятидневным прогнозом");
            while (cursor.moveToNext()) {
                if (cursor.getString(typeOfForecastColumnIndex).equals(ForecastContract.Forecasts.TYPE_OF_FORECAST_5DAY_FORECAST)) {
                    Gson gson = new Gson();
                    tmpForecastJson = cursor.getString(forecastJsonColumnIndex);
                    Log.i(TAG2, "Находясь в " + TAG + " в методе onPrepareDbFiveDayForecastCursorLoaderLoadFinished видим, что "
                            + "json с пятидневным прогнозом получился следующий: " + tmpForecastJson);
                    FiveDayForecast tmpForecast = gson.fromJson(tmpForecastJson, FiveDayForecast.class);
                    Log.i(TAG2, "Находясь в " + TAG + " в методе onPrepareDbFiveDayForecastCursorLoaderLoadFinished видим, что "
                            + "после преобразования из стринга в pojo мы получили следующий пятидневный прогноз: " +
                            "FiveDayForecast tmpForecast = " + tmpForecast.toString());
                    Log.i(TAG2, "Находясь в " + TAG + " в методе onPrepareDbFiveDayForecastCursorLoaderLoadFinished выполняем "
                            + "onFiveDayForecastReadyListener.onFiveDayForecastReady(tmpForecast);");
                    if (onFiveDayForecastReadyListener != null) {
                        onFiveDayForecastReadyListener.onFiveDayForecastReady(tmpForecast);
                    }
                }
            }
        }
    }

    @Override
    public void onLoadFinished(final Loader<Cursor> loader, final Cursor cursor) {
        switch (loader.getId()) {
            case PREPARE_DB_CURSOR_LOADER_ID:
                Log.i(TAG2, "Находимся в " + TAG + " в методе onLoadFinished, сейчас " +
                        "будем запускать: onPrepareDbCursorLoaderLoadFinished(cursor);");
                onPrepareDbCursorLoaderLoadFinished(cursor);
                break;
            case PREPARE_DB_FIVE_DAY_FORECAST_LOADER_ID:
                Log.i(TAG2, "Находимся в " + TAG + " в методе onLoadFinished, сейчас " +
                       "будем запускать: onPrepareDbFiveDayForecastCursorLoaderLoadFinished(cursor);");
                onPrepareDbFiveDayForecastCursorLoaderLoadFinished(cursor);
                break;
        }
     }

    @Override
    public void onLoaderReset(final Loader<Cursor> loader) {
    }

    public class AsyncTaskLoaderCallback implements LoaderManager.LoaderCallbacks<Boolean> {


        @Override
        public Loader<Boolean> onCreateLoader(final int id, final Bundle args) {
            switch (id) {
                case PREPARE_DB_DELETING_LOADER_ID:
                    Log.i(TAG, "Создаём " + ForecastDeleteLoader.class.getSimpleName());
                    return new ForecastDeleteLoader(starterApp, mDbHelper, args);
                case PREPARE_DB_SAVING_LOADER_ID:
                    Log.i(TAG, "Создаём " + ForecastSavingLoader.class.getSimpleName());
                    return new ForecastSavingLoader(starterApp, mDbHelper, args);
                    default:
                    return null;
            }
        }

        @Override
        public void onLoadFinished(final Loader<Boolean> loader, final Boolean data) {
            switch (loader.getId()) {
                case PREPARE_DB_DELETING_LOADER_ID:
                    if (data) {
                        Log.i(TAG, "Автоматически удалены старые пятидневные прогнозы.");
                    } else {
                        Log.i(TAG, "Не удалось автоматически удалить старые пятидневные прогнозы.");
                    }
                    mainMvpView.getLoaderManagerFromMainMvpView().destroyLoader(PREPARE_DB_DELETING_LOADER_ID);
                    break;

                case PREPARE_DB_SAVING_LOADER_ID:
                    if (data) {
                        Log.i(TAG, "Свежий прогноз сохранён");
                        // запускаем лоадер для получения не просроченного прогноза из базы
                        if (onFiveDayForecastReadyListener != null) {
                            startFiveDayForecastLoader();
                        }
                    } else {
                        Log.i(TAG, "Не удалось сохранить свежий прогноз.");
                    }
                    mainMvpView.getLoaderManagerFromMainMvpView().destroyLoader(PREPARE_DB_SAVING_LOADER_ID);
                    break;
            }
        }

        @Override
        public void onLoaderReset(final Loader<Boolean> loader) {

        }
    }

    public interface OnFiveDayForecastReadyListener {
        void onFiveDayForecastReady(FiveDayForecast fiveDayForecast);
    }
}
