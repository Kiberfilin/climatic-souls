package ru.cyber_eagle_owl.climaticsouls.model.blogic;

import android.location.Location;
import android.os.Bundle;

import java.util.LinkedList;

import ru.cyber_eagle_owl.climaticsouls.ui.screens.base.MvpView;

public interface BusinessLogic {

    boolean isStorageTimeForThisForecastExpired(final long forecastData);

    Bundle prepareSelectionForOld5DayForecastDeleting(LinkedList<Long> listOfOld5DayForecastsIds);

    Location getLastKnownLocation(String bestProvider);

    void setMvpView(final MvpView mvpView);

    String getBestProvider();

    Double getLatitudeFromLastKnownLocation();

    Double getLongitudeFromLastKnownLocation();
}
