package ru.cyber_eagle_owl.climaticsouls.model.api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.cyber_eagle_owl.climaticsouls.model.pojo.current.CurrentWeather;
import ru.cyber_eagle_owl.climaticsouls.model.pojo.forecast5day.FiveDayForecast;

public interface Api {

    @GET("weather")
    Call<CurrentWeather> getCurrentWeather(
            @Query("lat") double latitude,
            @Query("lon") double longitude,

            @Query("appid") String apiKey,
            @Query("units") String units
    );

    @GET("forecast")
    Call<FiveDayForecast> getFiveDayForecast(
            @Query("lat") double latitude,
            @Query("lon") double longitude,

            @Query("appid") String apiKey,
            @Query("units") String units
    );

}
