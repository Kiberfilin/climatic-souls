package ru.cyber_eagle_owl.climaticsouls.model.api;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.cyber_eagle_owl.climaticsouls.model.pojo.current.CurrentWeather;
import ru.cyber_eagle_owl.climaticsouls.model.pojo.forecast5day.FiveDayForecast;

public class ApiHelper {
    private final String TAG = "Weather/ApiHelper";
    private Api api;

    public ApiHelper() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.API_BASE_URL) // Базовый URL
                .addConverterFactory(GsonConverterFactory.create()) // Конвертер JSON
                .build();

        this.api = retrofit.create(Api.class);
    }

    public Call<CurrentWeather> getCallForCurrentWeather(double latitude, double longitude) {
        return api.getCurrentWeather(
                latitude,
                longitude,
                Constants.API_KEY,
                Constants.DEFAULT_UNITS
        );
    }

    public Call<FiveDayForecast> getCallFor5DayForecast(double latitude, double longitude) {
        return api.getFiveDayForecast(
                latitude,
                longitude,
                Constants.API_KEY,
                Constants.DEFAULT_UNITS
        );
    }
}
