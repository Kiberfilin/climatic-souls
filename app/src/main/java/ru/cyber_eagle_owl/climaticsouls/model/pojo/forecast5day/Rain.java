
package ru.cyber_eagle_owl.climaticsouls.model.pojo.forecast5day;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Rain implements Parcelable {

    @SerializedName("3h")
    @Expose
    private Double volumeForThreeHours;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Rain() {
    }

    /**
     * 
     * @param volumeForThreeHours Rain volume for last 3 hours, mm
     */
    public Rain(Double volumeForThreeHours) {
        super();
        this.volumeForThreeHours = volumeForThreeHours;
    }

    protected Rain(Parcel in) {
        if (in.readByte() == 0) {
            volumeForThreeHours = null;
        } else {
            volumeForThreeHours = in.readDouble();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (volumeForThreeHours == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(volumeForThreeHours);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Rain> CREATOR = new Creator<Rain>() {
        @Override
        public Rain createFromParcel(Parcel in) {
            return new Rain(in);
        }

        @Override
        public Rain[] newArray(int size) {
            return new Rain[size];
        }
    };

    public Double getVolumeForThreeHours() {
        return volumeForThreeHours;
    }

    public void setVolumeForThreeHours(Double volumeForThreeHours) {
        this.volumeForThreeHours = volumeForThreeHours;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("volumeForThreeHours", volumeForThreeHours).toString();
    }
}
