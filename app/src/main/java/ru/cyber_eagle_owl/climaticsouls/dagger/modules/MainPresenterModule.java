package ru.cyber_eagle_owl.climaticsouls.dagger.modules;

import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;
import ru.cyber_eagle_owl.climaticsouls.dagger.annotations.PerActivity;
import ru.cyber_eagle_owl.climaticsouls.model.DataManager;
import ru.cyber_eagle_owl.climaticsouls.ui.screens.main.MainPresenter;

@Module
public class MainPresenterModule {

    @Provides
    @NonNull
    @PerActivity
    MainPresenter provideMainPresenter(DataManager dataManager) {
        return new MainPresenter(dataManager);
    }
}
