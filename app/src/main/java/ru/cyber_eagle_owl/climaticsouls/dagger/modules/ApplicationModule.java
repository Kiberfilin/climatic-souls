package ru.cyber_eagle_owl.climaticsouls.dagger.modules;

import android.app.Application;
import android.content.Context;
import dagger.Module;
import dagger.Provides;
import ru.cyber_eagle_owl.climaticsouls.dagger.annotations.ApplicationContext;

@Module
public class ApplicationModule {

    private final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }
}
