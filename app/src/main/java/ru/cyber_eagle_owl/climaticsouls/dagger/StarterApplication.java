package ru.cyber_eagle_owl.climaticsouls.dagger;

import android.app.Application;
import android.content.Context;

import ru.cyber_eagle_owl.climaticsouls.dagger.components.ApplicationComponent;
import ru.cyber_eagle_owl.climaticsouls.dagger.components.DaggerApplicationComponent;
import ru.cyber_eagle_owl.climaticsouls.dagger.modules.ApiHelperModule;
import ru.cyber_eagle_owl.climaticsouls.dagger.modules.ApplicationModule;
import ru.cyber_eagle_owl.climaticsouls.dagger.modules.DataManagerModule;

public class StarterApplication extends Application {

    protected ApplicationComponent mApplicationComponent;

    public static StarterApplication get(Context context) {
        return (StarterApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .dataManagerModule(new DataManagerModule())
                .apiHelperModule(new ApiHelperModule())
                .build();
        //mApplicationComponent.inject(this);
    }
    public ApplicationComponent getApplicationComponent() {
        return mApplicationComponent;
    }
}
