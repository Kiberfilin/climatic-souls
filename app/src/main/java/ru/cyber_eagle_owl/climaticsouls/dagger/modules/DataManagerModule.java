package ru.cyber_eagle_owl.climaticsouls.dagger.modules;

import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.cyber_eagle_owl.climaticsouls.dagger.annotations.ApplicationContext;
import ru.cyber_eagle_owl.climaticsouls.model.DataManager;
import ru.cyber_eagle_owl.climaticsouls.model.api.ApiHelper;

import static android.content.Context.MODE_PRIVATE;

@Module
public class DataManagerModule {

    @Provides
    @Singleton
    @NonNull
    public DataManager provideDataManager(@ApplicationContext Context starterApp, ApiHelper apiHelper) {
        return new DataManager(starterApp.getSharedPreferences("weather", MODE_PRIVATE), starterApp, apiHelper);
    }
}
