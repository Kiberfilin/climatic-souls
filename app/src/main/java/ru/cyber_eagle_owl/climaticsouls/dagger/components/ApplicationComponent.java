package ru.cyber_eagle_owl.climaticsouls.dagger.components;

import javax.inject.Singleton;

import dagger.Component;
import ru.cyber_eagle_owl.climaticsouls.dagger.StarterApplication;
import ru.cyber_eagle_owl.climaticsouls.dagger.modules.ApiHelperModule;
import ru.cyber_eagle_owl.climaticsouls.dagger.modules.ApplicationModule;
import ru.cyber_eagle_owl.climaticsouls.dagger.modules.DataManagerModule;
import ru.cyber_eagle_owl.climaticsouls.model.DataManager;
import ru.cyber_eagle_owl.climaticsouls.services.WeatherJobService;
import ru.cyber_eagle_owl.climaticsouls.services.WeatherService;

@Singleton
@Component(modules = {ApplicationModule.class, DataManagerModule.class, ApiHelperModule.class})
public interface ApplicationComponent {

    DataManager getDataManager();

    void inject(StarterApplication application);

    void inject(WeatherService weatherService);

    void inject(WeatherJobService weatherJobService);

}
