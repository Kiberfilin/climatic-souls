package ru.cyber_eagle_owl.climaticsouls.dagger.modules;

import dagger.Module;
import dagger.Provides;
import ru.cyber_eagle_owl.climaticsouls.model.api.ApiHelper;

@Module
public class ApiHelperModule {

    @Provides
    ApiHelper provideApiHelper() {
        return new ApiHelper();
    }
}
