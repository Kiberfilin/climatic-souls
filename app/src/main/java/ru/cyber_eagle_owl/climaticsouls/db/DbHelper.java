package ru.cyber_eagle_owl.climaticsouls.db;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class DbHelper {

    private ContentResolver mContentResolver;

    public DbHelper(Context mMvpView) {
        mContentResolver = mMvpView.getContentResolver();
    }

    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection,
                        @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        return mContentResolver.query(uri, projection, selection, selectionArgs, sortOrder);
    }

    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        return mContentResolver.insert(uri, contentValues);
    }

    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return mContentResolver.delete(uri, selection, selectionArgs);
    }

    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues,
                      @Nullable String selection, @Nullable String[] selectionArgs) {
        return mContentResolver.update(uri, contentValues, selection, selectionArgs);
    }
}
