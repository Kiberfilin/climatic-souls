package ru.cyber_eagle_owl.climaticsouls.ui.screens.main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.LoaderManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.JobTrigger;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.RetryStrategy;
import com.firebase.jobdispatcher.Trigger;

import java.util.ArrayList;

import javax.inject.Inject;

import ru.cyber_eagle_owl.climaticsouls.R;
import ru.cyber_eagle_owl.climaticsouls.dagger.StarterApplication;
import ru.cyber_eagle_owl.climaticsouls.dagger.components.ActivityComponent;
import ru.cyber_eagle_owl.climaticsouls.dagger.components.DaggerActivityComponent;
import ru.cyber_eagle_owl.climaticsouls.dagger.modules.ActivityModule;
import ru.cyber_eagle_owl.climaticsouls.dagger.modules.MainPresenterModule;
import ru.cyber_eagle_owl.climaticsouls.model.pojo.forecast5day.Measurements;
import ru.cyber_eagle_owl.climaticsouls.services.WeatherJobService;
import ru.cyber_eagle_owl.climaticsouls.services.WeatherService;
import ru.cyber_eagle_owl.climaticsouls.ui.screens.base.BaseActivity;
import ru.cyber_eagle_owl.climaticsouls.ui.screens.dailyDetails.DailyDetailsActivity;
import ru.cyber_eagle_owl.climaticsouls.ui.adapters.WeatherRecyclerViewAdapter;
import ru.cyber_eagle_owl.climaticsouls.ui.fragments.DayWeatherFragment;
import ru.cyber_eagle_owl.climaticsouls.ui.fragments.ListingFragment;

public class MainActivity extends BaseActivity implements MainMvpView, WeatherRecyclerViewAdapter.OnDailyWeatherItemClickListener {
    //todo 1) сделать сервис на JobService
    //todo 2) если min и max температуры равны, то выводить одно значение
    private static final int REQUEST_CODE_LOCATION_PERMISSION = 0;
    private static final String TAG = "Weather/MainActivity";
    private static final String LISTING_FRAGMENT_TAG = "listFragment";
    private static final String DAY_WEATHER_FRAGMENT_TAG = "dayWeatherFragment";
    private static final String WEATHER_JOBSERVICE_TAG = "WeatherJobService";

    private static long backPressed;

    private static boolean isTablet;
    private Toolbar toolbar;

    private android.app.FragmentManager mFragmentManager;

    @Inject
    MainPresenter mPresenter;

    private ActivityComponent mActivityComponent;

    private Toast currentToast;

    public ActivityComponent getActivityComponent() {
        if (mActivityComponent == null) {
            mActivityComponent = DaggerActivityComponent.builder()
                    .mainPresenterModule(new MainPresenterModule())
                    .activityModule(new ActivityModule(this))
                    .applicationComponent(StarterApplication.get(this).getApplicationComponent())
                    .build();
        }
        return mActivityComponent;
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public MainPresenter getmPresenter() {
        return mPresenter;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getActivityComponent().inject(this);

        isTablet = getResources().getBoolean(R.bool.isTablet);
        mFragmentManager = getFragmentManager();
        android.app.FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();

        if (mFragmentManager.findFragmentByTag(LISTING_FRAGMENT_TAG) == null) {
            ListingFragment listingFragment = ListingFragment.newInstance();
            fragmentTransaction.add(R.id.mainFragmentContainer, listingFragment, LISTING_FRAGMENT_TAG);
            fragmentTransaction.commit();
        }

        mPresenter.onAttach(this);

        final CollapsingToolbarLayout collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(R.string.toolbar_title);

        if (isTablet) {
            toolbar.setBackgroundResource(R.color.colorPrimary);
        } else {
            collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.CollapsedToolbar);
        }

        checkAndRequestGeoPermission();
        Log.i(TAG, "Last update: " + mPresenter.getLastUpdate());
    }

    @Override
    public void startWeatherService() {
        //Intent intent = new Intent(this, WeatherService.class);
        //startService(intent);
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));

        Job serviceJob = dispatcher.newJobBuilder()
                .setService(WeatherJobService.class)
                .setTag(WEATHER_JOBSERVICE_TAG)
                .setReplaceCurrent(false)
                .setTrigger(Trigger.executionWindow(10, 60))
                .setLifetime(Lifetime.FOREVER)
                .setRecurring(true)
                .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                .build();

        dispatcher.mustSchedule(serviceJob);
    }

    @Override
    public void checkAndRequestGeoPermission() {
        int permissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_CODE_LOCATION_PERMISSION);
        } else {
            startWeatherService();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_LOCATION_PERMISSION) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startWeatherService();
            } else {
                // Нет гео
                // Попробуем показать ещё раз
                checkAndRequestGeoPermission();
            }
        }
    }

    @Override
    public void onShortClick(final ArrayList<Measurements> dailyMeasurementsToShow) {
        Log.i(TAG, "ArrayList с измерениями на день == " + dailyMeasurementsToShow);
        if (isTablet) {
            // если у нас планшет делаем новый фрагмент на том же экране
            android.app.FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
            DayWeatherFragment dayWeatherFragment = DayWeatherFragment.newInstance();
            dayWeatherFragment.setDailyMeasurementsToShow(dailyMeasurementsToShow);
            if (mFragmentManager.findFragmentByTag(DAY_WEATHER_FRAGMENT_TAG) == null) {
                fragmentTransaction.add(R.id.dayWeatherFragmentContainer, dayWeatherFragment, DAY_WEATHER_FRAGMENT_TAG);
            } else {
                fragmentTransaction.replace((R.id.dayWeatherFragmentContainer), dayWeatherFragment, DAY_WEATHER_FRAGMENT_TAG);
            }
            fragmentTransaction.commit();
        } else {
            // если телефон, то открываем детали прогноза в новой активити
            Intent intent = new Intent(this, DailyDetailsActivity.class);
            intent.putParcelableArrayListExtra(DailyDetailsActivity.TAG_DAILY_MEASUREMENTS, dailyMeasurementsToShow);
            startActivity(intent);
        }
    }

    @Override
    public LoaderManager getLoaderManagerFromMainMvpView() {
        return getLoaderManager();
    }

    @Override
    public LocationManager getLocationManagerFromMainMvpView() {
        return (LocationManager) getSystemService(LOCATION_SERVICE);
    }

    @Override
    public void showToastMsg(final String textMsg) {
        if (currentToast != null) {
            currentToast.cancel();
        }
        currentToast = Toast.makeText(this, textMsg, Toast.LENGTH_SHORT);
        currentToast.show();
    }

    @Override
    public void onBackPressed() {
        if (backPressed + 2000 > System.currentTimeMillis()){
            if (currentToast != null) {
                currentToast.cancel();
            }
            super.onBackPressed();
        } else {
            showToastMsg(getString(R.string.doble_back_notify));
            backPressed = System.currentTimeMillis();
        }
    }
}
