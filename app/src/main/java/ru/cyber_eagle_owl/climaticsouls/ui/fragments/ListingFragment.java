package ru.cyber_eagle_owl.climaticsouls.ui.fragments;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.cyber_eagle_owl.climaticsouls.R;
import ru.cyber_eagle_owl.climaticsouls.model.DataManager;
import ru.cyber_eagle_owl.climaticsouls.model.pojo.current.CurrentWeather;
import ru.cyber_eagle_owl.climaticsouls.model.pojo.forecast5day.FiveDayForecast;
import ru.cyber_eagle_owl.climaticsouls.ui.screens.main.MainActivity;
import ru.cyber_eagle_owl.climaticsouls.ui.screens.main.MainPresenter;
import ru.cyber_eagle_owl.climaticsouls.ui.adapters.WeatherRecyclerViewAdapter;

public class ListingFragment extends Fragment {
    public static final String TAG_FOR_CURRENT_WEATHER_FROM_SERVICE = "CurrentWeather from service";
    private RecyclerView weatherRecyclerView;
    private WeatherRecyclerViewAdapter mWeatherRecyclerViewAdapter;
    private final WeatherReceiver weatherReceiver = new WeatherReceiver();
    private static final String TAG = "Weather/ListingFragment";
    private static final String TAG2 = "0==(========>";

    private static final String TAG_FOR_FORECAST_SAVING = "FiveDayForecast";
    private static final String TAG_FOR_WEATHER_SAVING = "CurrentWeather";
    private static final String TAG_FOR_COLORS_SAVING = "Colors";

    public static final String ACTION_GOT_WEATHER = "com.skillberg.weather2.ACTION_GOT_WEATHER";
    public static final String EXTRA_CURRENT_WEATHER = "current_weather";

    private MainPresenter mainPresenter;

    private CurrentWeather savedCurentWeather;
    private FiveDayForecast savedFiveDayForecast;
    private Bundle colors;

    private DataManager.OnFiveDayForecastReadyListener listener = new DataManager.OnFiveDayForecastReadyListener() {
        @Override
        public void onFiveDayForecastReady(final FiveDayForecast fiveDayForecast) {
            Log.i(TAG2, "Находясь в " + TAG + " в методе onFiveDayForecastReady нового DataManager.OnFiveDayForecastReadyListener() " +
                    "который мы создаём, чтобы проинициализировать DataManager.OnFiveDayForecastReadyListener listener, мы делаем следующее: " +
                    "mWeatherRecyclerViewAdapter.setFiveDayForecast(fiveDayForecast);\n" +
                    "            mWeatherRecyclerViewAdapter.notifyDataSetChanged();\n");
            mWeatherRecyclerViewAdapter.setFiveDayForecast(fiveDayForecast);
            mWeatherRecyclerViewAdapter.notifyDataSetChanged();
            savedFiveDayForecast = fiveDayForecast;
        }
    };

    public static ListingFragment newInstance() {
        return new ListingFragment();
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_listing, container, false);
        mainPresenter = ((MainActivity) getActivity()).getmPresenter();
        weatherRecyclerView = view.findViewById(R.id.weather_rv);
        weatherRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mWeatherRecyclerViewAdapter = new WeatherRecyclerViewAdapter(getActivity().getApplicationContext());
        mWeatherRecyclerViewAdapter.setOnDailyWeatherItemClickListener((MainActivity) getActivity());
        weatherRecyclerView.setAdapter(mWeatherRecyclerViewAdapter);
        mWeatherRecyclerViewAdapter.notifyDataSetChanged();

        if (savedInstanceState != null) {
            Log.i(TAG2, "savedInstanceState = " + savedInstanceState);
            CurrentWeather tmpCurrentWeather = savedInstanceState.getParcelable(TAG_FOR_WEATHER_SAVING);
            FiveDayForecast tmpFiveDayForecast = savedInstanceState.getParcelable(TAG_FOR_FORECAST_SAVING);
            Bundle tmpColors = savedInstanceState.getBundle(TAG_FOR_COLORS_SAVING);

            if (tmpCurrentWeather != null) {
                savedCurentWeather = tmpCurrentWeather;
                mWeatherRecyclerViewAdapter.setCurrentWeather(tmpCurrentWeather);
                mWeatherRecyclerViewAdapter.notifyDataSetChanged();
            }

            if (tmpColors != null) {
                colors = tmpColors;
                mWeatherRecyclerViewAdapter.setColorsBundle(colors);
            } else {
                colors = new Bundle();
                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.collapsingimg);

                Palette.from(bitmap)
                        .generate(new Palette.PaletteAsyncListener() {
                            @Override
                            public void onGenerated(@NonNull final Palette palette) {
                                Palette.Swatch textSwatch = palette.getVibrantSwatch();
                                if (textSwatch == null) {
                                    Log.i(TAG, "Null swatch :(");
                                } else {
                                    colors.putInt(WeatherRecyclerViewAdapter.KEY_PALETTE_BACKGROUND_COLOR,
                                            textSwatch.getRgb());
                                    colors.putInt(WeatherRecyclerViewAdapter.KEY_PALETTE_TEXT_COLOR,
                                            textSwatch.getTitleTextColor());
                                    mWeatherRecyclerViewAdapter.setColorsBundle(colors);
                                    mWeatherRecyclerViewAdapter.notifyDataSetChanged();
                                }
                            }
                        });
            }

            if (tmpFiveDayForecast != null) {
                savedFiveDayForecast = tmpFiveDayForecast;
                mWeatherRecyclerViewAdapter.setFiveDayForecast(tmpFiveDayForecast);
                mWeatherRecyclerViewAdapter.notifyDataSetChanged();
                } else {
                Log.i(TAG2, "Запускаем mainPresenter.prepareDb(listener) из " + TAG + " а listener = "
                        + listener);
                mainPresenter.prepareDb(listener);
            }
        } else {
            Log.i(TAG2, "Запускаем mainPresenter.prepareDb(listener) из " + TAG + " а listener = "
                    + listener);
            mainPresenter.prepareDb(listener);
            if (savedCurentWeather == null) {
                Log.i(TAG2, "getActivity().getIntent() = " + getActivity().getIntent());
                CurrentWeather tmpCurrentWeather = getActivity().getIntent()
                        .getParcelableExtra(TAG_FOR_CURRENT_WEATHER_FROM_SERVICE);
                if (tmpCurrentWeather != null) {
                    savedCurentWeather = tmpCurrentWeather;
                    mWeatherRecyclerViewAdapter.setCurrentWeather(tmpCurrentWeather);
                    mWeatherRecyclerViewAdapter.notifyDataSetChanged();
                }
                if (colors == null) {
                    colors = new Bundle();
                    Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.collapsingimg);

                    Palette.from(bitmap)
                            .generate(new Palette.PaletteAsyncListener() {
                                @Override
                                public void onGenerated(@NonNull final Palette palette) {
                                    Palette.Swatch textSwatch = palette.getVibrantSwatch();
                                    if (textSwatch == null) {
                                        Log.i(TAG, "Null swatch :(");
                                    } else {
                                        colors.putInt(WeatherRecyclerViewAdapter.KEY_PALETTE_BACKGROUND_COLOR,
                                                textSwatch.getRgb());
                                        colors.putInt(WeatherRecyclerViewAdapter.KEY_PALETTE_TEXT_COLOR,
                                                textSwatch.getTitleTextColor());
                                        mWeatherRecyclerViewAdapter.setColorsBundle(colors);
                                        mWeatherRecyclerViewAdapter.notifyDataSetChanged();
                                    }
                                }
                            });
                }
            }
        }
        return view;
    }

    private class WeatherReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ACTION_GOT_WEATHER.equals(intent.getAction())) {
                Log.i(TAG, "Got weather!");
                savedCurentWeather = intent.getParcelableExtra(EXTRA_CURRENT_WEATHER);
                Log.i(TAG, "Current Weather = " + savedCurentWeather);
                mWeatherRecyclerViewAdapter.setCurrentWeather(savedCurentWeather);
                mWeatherRecyclerViewAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(TAG_FOR_WEATHER_SAVING, savedCurentWeather);
        outState.putParcelable(TAG_FOR_FORECAST_SAVING, savedFiveDayForecast);
        outState.putBundle(TAG_FOR_COLORS_SAVING, colors);
    }

    @Override
    public void onStart() {
        super.onStart();

        IntentFilter intentFilter = new IntentFilter(ACTION_GOT_WEATHER);
        getActivity().registerReceiver(weatherReceiver, intentFilter);
    }

    @Override
    public void onStop() {
        getActivity().unregisterReceiver(weatherReceiver);

        super.onStop();
    }
}
