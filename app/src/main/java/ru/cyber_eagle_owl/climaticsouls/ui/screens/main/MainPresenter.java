package ru.cyber_eagle_owl.climaticsouls.ui.screens.main;

import android.support.annotation.Nullable;
import android.util.Log;

import ru.cyber_eagle_owl.climaticsouls.model.DataManager;
import ru.cyber_eagle_owl.climaticsouls.ui.screens.base.BasePresenter;

public class MainPresenter<V extends MainMvpView> extends BasePresenter<V> implements MainMvpPresenter<V> {

    private DataManager mDataManager;
    private static final String TAG = "Weather/MainPresenter";
    private static final String TAG2 = "0==(========>";

    public MainPresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }

    @Override
    public Long getLastUpdate() {
        return mDataManager.getLastUpdate();
    }

    @Override
    public void saveForecastToDb(final String typeOfForecast, final String forecastJson) {
        mDataManager.insert(typeOfForecast, forecastJson);
    }

    @Override
    public void prepareDb(@Nullable DataManager.OnFiveDayForecastReadyListener listener) {
        if (listener != null) {
            Log.i(TAG2, "Находясь в " + TAG + " в методе prepareDb и имея listener != null" +
                    " запускаем mDataManager.setOnFiveDayForecastReadyListener(listener)");
            mDataManager.setOnFiveDayForecastReadyListener(listener);
        } else {
            Log.i(TAG2, "Находясь в " + TAG + " в методе prepareDb мы имеем: OnFiveDayForecastReadyListener = "
                    + listener + "!!!");
        }
        if (mMvpView == null) {
            Log.i(TAG2, "Находясь в " + TAG + " в методе prepareDb мы имеем: mMvpView = " + mMvpView + "!!!");
        } else {
            Log.i(TAG2, "Находясь в " + TAG + " в методе prepareDb мы имеем: mMvpView != null, теперь будем выполнять: "
            + "mDataManager.setMainMvpView(mMvpView); а потом: mDataManager.prepareDb();");
            mDataManager.setMainMvpView(mMvpView);
            mDataManager.prepareDb();
        }
    }
}
