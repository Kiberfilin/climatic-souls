package ru.cyber_eagle_owl.climaticsouls.ui.screens.dailyDetails;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import java.util.ArrayList;

import ru.cyber_eagle_owl.climaticsouls.R;
import ru.cyber_eagle_owl.climaticsouls.model.pojo.forecast5day.Measurements;
import ru.cyber_eagle_owl.climaticsouls.ui.screens.base.BaseActivity;
import ru.cyber_eagle_owl.climaticsouls.ui.fragments.DayWeatherFragment;

public class DailyDetailsActivity extends BaseActivity implements DailyDetailsMvpView {

    private android.app.FragmentManager fragmentManager;
    private static final String TAG = "Weather/DailyDetailsActivity";
    private static final String DAY_WEATHER_FRAGMENT_TAG = "dayWeatherFragment";

    public static final String TAG_DAILY_MEASUREMENTS = "dailyMeasurements";
    public static final String TAG_DAILY_MEASUREMENTS_TO_SAVE_STATE = "dailyMeasurementsSaveState";

    private ArrayList<Measurements> dailyMeasurementsToShow;

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(TAG_DAILY_MEASUREMENTS_TO_SAVE_STATE, dailyMeasurementsToShow);
    }

    @SuppressLint("LongLogTag")
    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_details);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundResource(R.color.colorPrimary);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle(R.string.toolbar_title_for_daily_forecast);

        if (savedInstanceState == null) {
            Intent intent = getIntent();
            Log.i(TAG, "Intent == " + intent);
                dailyMeasurementsToShow = intent.getParcelableArrayListExtra(TAG_DAILY_MEASUREMENTS);
        } else {
            dailyMeasurementsToShow = savedInstanceState.getParcelableArrayList(TAG_DAILY_MEASUREMENTS_TO_SAVE_STATE);
        }

        fragmentManager = getFragmentManager();
        android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        DayWeatherFragment dayWeatherFragment = DayWeatherFragment.newInstance();
        dayWeatherFragment.setDailyMeasurementsToShow(dailyMeasurementsToShow);
        if (fragmentManager.findFragmentByTag(DAY_WEATHER_FRAGMENT_TAG) == null) {
            fragmentTransaction.add(R.id.mainFragmentContainer, dayWeatherFragment, DAY_WEATHER_FRAGMENT_TAG);
        } else {
            fragmentTransaction.replace((R.id.mainFragmentContainer), dayWeatherFragment, DAY_WEATHER_FRAGMENT_TAG);
        }
        fragmentTransaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
