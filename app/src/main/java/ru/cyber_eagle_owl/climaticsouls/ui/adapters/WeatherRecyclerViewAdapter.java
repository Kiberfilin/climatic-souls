package ru.cyber_eagle_owl.climaticsouls.ui.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import ru.cyber_eagle_owl.climaticsouls.R;
import ru.cyber_eagle_owl.climaticsouls.model.pojo.current.CurrentWeather;
import ru.cyber_eagle_owl.climaticsouls.model.pojo.forecast5day.FiveDayForecast;
import ru.cyber_eagle_owl.climaticsouls.model.pojo.forecast5day.Measurements;

public class WeatherRecyclerViewAdapter extends RecyclerView.Adapter<WeatherRecyclerViewAdapter.WeatherViewHolder> {

    private static final int CURRENT_WEATHER_ITEM = 0;
    private static final int DAILY_WEATHER_ITEM_1 = 1;
    private static final int DAILY_WEATHER_ITEM_2 = 2;
    private static final int DAILY_WEATHER_ITEM_3 = 3;
    private static final int DAILY_WEATHER_ITEM_4 = 4;
    private static final int DAILY_WEATHER_ITEM_5 = 5;
    private static final int CURRENT_WEATHER_ITEM_POSITION = 0;
    private static final int DAILY_WEATHER_ITEM_1_POSITION = 1;
    private static final int DAILY_WEATHER_ITEM_2_POSITION = 2;
    private static final int DAILY_WEATHER_ITEM_3_POSITION = 3;
    private static final int DAILY_WEATHER_ITEM_4_POSITION = 4;
    private static final int DAILY_WEATHER_ITEM_5_POSITION = 5;

    private static final String TAG = "Weather/WeatherRecyclerViewAdapter";
    private static final String TAG2 = "0==(========>";

    public static final String KEY_PALETTE_BACKGROUND_COLOR = "CurrentWeatherItemBacground";
    public static final String KEY_PALETTE_TEXT_COLOR = "CurrentWeatherItemText";

    @Nullable
    private Bundle colorsBundle;

    @Nullable
    private CurrentWeather currentWeather;
    @Nullable
    private ArrayList<ArrayList<Measurements>> listsOfDailyMeasurements;

    private Context context;

    private OnDailyWeatherItemClickListener onDailyWeatherItemClickListener;

    public WeatherRecyclerViewAdapter(Context context) {
        this.context = context;
    }

    public void setCurrentWeather(CurrentWeather currentWeather) {
        this.currentWeather = currentWeather;
    }

    public void setFiveDayForecast(@NonNull FiveDayForecast fiveDayForecast) {
        int tmpDayOfMonth = 0;
        ArrayList<Measurements> tmpListOfMeasurementsPerOneDay = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        int lastDailyForecastItem = fiveDayForecast.getMeasurementsCount() - 1;

        if (listsOfDailyMeasurements == null) {
            listsOfDailyMeasurements = new ArrayList<>();
        } else {
            listsOfDailyMeasurements.clear();
        }

        for (int i = 0; i < fiveDayForecast.getMeasurementsCount(); i++) {
            switch (i) {
                case 0:
                    calendar.setTimeInMillis(fiveDayForecast.getMeasurementsList().get(i).getForecastDataUnixStyle() * 1000);
                    tmpDayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                    tmpListOfMeasurementsPerOneDay.add(fiveDayForecast.getMeasurementsList().get(i));
                    break;
                default:
                    calendar.setTimeInMillis(fiveDayForecast.getMeasurementsList().get(i).getForecastDataUnixStyle() * 1000);
                    if (tmpDayOfMonth == calendar.get(Calendar.DAY_OF_MONTH)) {
                        tmpListOfMeasurementsPerOneDay.add(fiveDayForecast.getMeasurementsList().get(i));
                        if (i == lastDailyForecastItem) {
                            listsOfDailyMeasurements.add(tmpListOfMeasurementsPerOneDay);
                            Log.i(TAG2, "Находясь в " + TAG + " в методе setFiveDayForecast мы собрали измерения за день №" +
                                    tmpDayOfMonth + ". Получился ArrayList tmpListOfMeasurementsPerOneDay размером " + tmpListOfMeasurementsPerOneDay.size() +
                                    " который равен: " + tmpListOfMeasurementsPerOneDay);
                        }
                    } else {
                        listsOfDailyMeasurements.add(tmpListOfMeasurementsPerOneDay);
                        Log.i(TAG2, "Находясь в " + TAG + " в методе setFiveDayForecast мы собрали измерения за день №" +
                                tmpDayOfMonth + ". Получился ArrayList tmpListOfMeasurementsPerOneDay размером " + tmpListOfMeasurementsPerOneDay.size() +
                                " который равен: " + tmpListOfMeasurementsPerOneDay);
                        if (i != lastDailyForecastItem) {
                            tmpListOfMeasurementsPerOneDay = new ArrayList<>();
                            tmpDayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                            tmpListOfMeasurementsPerOneDay.add(fiveDayForecast.getMeasurementsList().get(i));
                        }
                    }
                    break;
            }
        }
    }

    // В этом методе мы создаем новую ячейку
    @NonNull
    @Override
    public WeatherViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View view;

        switch (viewType) {
            case CURRENT_WEATHER_ITEM:
                view = layoutInflater.inflate(R.layout.card_current_weather, parent, false);
                view.setTag(R.string.view_type_tag, CURRENT_WEATHER_ITEM);
                break;

            default:
                view = layoutInflater.inflate(R.layout.card_day_weather, parent, false);
                view.setTag(R.string.view_type_tag, viewType);
        }

        return new WeatherViewHolder(view);
    }

    // В этом методе мы привязываем данные к ячейке
    @Override
    public void onBindViewHolder(@NonNull final WeatherViewHolder holder, final int position) {
        switch (position) {
            case CURRENT_WEATHER_ITEM_POSITION:
                //вставляем данные в текствьюхи
                aplyColors(holder.currentWeatherTV, holder.currentWeatherCardLayout, colorsBundle);
                if (currentWeather == null) {
                    holder.currentWeatherTV.setText(R.string.text_updating_weather);
                } else {
                    holder.currentWeatherTV.setText(context.getString(
                            R.string.title_current_weather,
                            (int) currentWeather.getMain().getTemp(),
                            currentWeather.getCityName()));
                }
                break;

            default:
                if (listsOfDailyMeasurements != null) {
                    Measurements tmpDailyMeasurements;
                    ArrayList<Measurements> tmpListOfDailyMeasurements = listsOfDailyMeasurements.get(position - 1);
                    // чтобы выбрать измерение, наиболее близкое к 12:00 дня, отсортируем tmpListOfDailyMeasurements
                    // при помощи специального компаратора, и возьмём первый элемент, как наиболее близкий к 12:00
                    Collections.sort(tmpListOfDailyMeasurements, new MeasurementsComparator());
                    tmpDailyMeasurements = tmpListOfDailyMeasurements.get(0);
                    if (tmpDailyMeasurements != null) {
                        holder.dateTV.setText(holder.simpleDateFormat.format(new Date(
                                tmpDailyMeasurements.getForecastDataUnixStyle() * 1000)));
                        holder.temperatureTV.setText(context.getString(
                                R.string.temperature,
                                tmpDailyMeasurements.getMain().getTempMin(),
                                tmpDailyMeasurements.getMain().getTempMax()));
                    } else {
                        holder.dateTV.setText(R.string.title_updating_weather);
                        holder.temperatureTV.setText(R.string.text_updating_weather);
                    }
                } else {
                    holder.dateTV.setText(R.string.title_updating_weather);
                    holder.temperatureTV.setText(R.string.text_updating_weather);
                }
        }
    }

    private void aplyColors(final TextView currentWeatherTV, final LinearLayout currentWeatherCardLayout,
                            final Bundle colorsBundle) {
        if (colorsBundle != null) {
            currentWeatherCardLayout.setBackgroundColor(colorsBundle.getInt(KEY_PALETTE_BACKGROUND_COLOR));
            currentWeatherTV.setTextColor(colorsBundle.getInt(KEY_PALETTE_TEXT_COLOR));
        }
    }

    // В этом методе мы возвращаем количество элементов списка
    @Override
    public int getItemCount() {
        return 6;
    }

    @Override
    public int getItemViewType(final int position) {
        switch (position) {
            case CURRENT_WEATHER_ITEM_POSITION:
                return CURRENT_WEATHER_ITEM;
            case DAILY_WEATHER_ITEM_1_POSITION:
                return DAILY_WEATHER_ITEM_1;
            case DAILY_WEATHER_ITEM_2_POSITION:
                return DAILY_WEATHER_ITEM_2;
            case DAILY_WEATHER_ITEM_3_POSITION:
                return DAILY_WEATHER_ITEM_3;
            case DAILY_WEATHER_ITEM_4_POSITION:
                return DAILY_WEATHER_ITEM_4;
            case DAILY_WEATHER_ITEM_5_POSITION:
                return DAILY_WEATHER_ITEM_5;
            default:
                return -1;
        }
    }

    public void setOnDailyWeatherItemClickListener(final WeatherRecyclerViewAdapter
            .OnDailyWeatherItemClickListener onDailyWeatherItemClickListener) {
        this.onDailyWeatherItemClickListener = onDailyWeatherItemClickListener;
    }

    public void setColorsBundle(@Nullable final Bundle colorsBundle) {
        this.colorsBundle = colorsBundle;
    }

    public class WeatherViewHolder extends RecyclerView.ViewHolder {
        public final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEE, dd MMMM", Locale.ENGLISH);
        private TextView currentWeatherTV;
        private TextView dateTV;
        private TextView temperatureTV;
        private LinearLayout currentWeatherCardLayout;
        public WeatherViewHolder(final View itemView) {
            super(itemView);
            switch ((int) itemView.getTag(R.string.view_type_tag)) {
                case CURRENT_WEATHER_ITEM:
                    //ищем элементы в layout card_current_weather
                    currentWeatherTV = itemView.findViewById(R.id.currentWeatherTV);
                    currentWeatherCardLayout = itemView.findViewById(R.id.currentWeatherCardLayout);
                    break;
                default:
                    //ищем элементы в layout card_day_weather
                    dateTV = itemView.findViewById(R.id.dateTV);
                    temperatureTV = itemView.findViewById(R.id.temperatureTV);
                    itemView.setOnClickListener(new View.OnClickListener() {
                        @SuppressLint("LongLogTag")
                        @Override
                        public void onClick(final View v) {
                            if (onDailyWeatherItemClickListener != null && listsOfDailyMeasurements != null) {
                                ArrayList<Measurements> dailyMeasurementsToShow = listsOfDailyMeasurements
                                        .get((int) itemView.getTag(R.string.view_type_tag) - 1);
                                onDailyWeatherItemClickListener.onShortClick(dailyMeasurementsToShow);
                            } else {
                                Log.e(TAG, "onDailyWeatherItemClickListener = " + onDailyWeatherItemClickListener +
                                        "; listsOfDailyMeasurements = " + listsOfDailyMeasurements);
                            }
                        }
                    });
            }
        }
    }

    public interface OnDailyWeatherItemClickListener {
        void onShortClick(ArrayList<Measurements> dailyMeasurementsToShow);
    }
    public class MeasurementsComparator implements Comparator<Measurements> {

        @Override
        public int compare(final Measurements o1, final Measurements o2) {
            final int HOUR_OF_MEASUREMENT = 12;
            int hourOfDay1, hourOfDay2;

            Calendar calendar = Calendar.getInstance();

            calendar.setTimeInMillis(o1.getForecastDataUnixStyle() * 1000);
            hourOfDay1 = calendar.get(Calendar.HOUR_OF_DAY);
            calendar.setTimeInMillis(o2.getForecastDataUnixStyle() * 1000);
            hourOfDay2 = calendar.get(Calendar.HOUR_OF_DAY);

            return Math.abs(hourOfDay1 - HOUR_OF_MEASUREMENT) - Math.abs(hourOfDay2 - HOUR_OF_MEASUREMENT);
        }
    }
}
