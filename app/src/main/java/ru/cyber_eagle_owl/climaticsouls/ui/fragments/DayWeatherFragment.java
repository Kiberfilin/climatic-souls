package ru.cyber_eagle_owl.climaticsouls.ui.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ru.cyber_eagle_owl.climaticsouls.R;
import ru.cyber_eagle_owl.climaticsouls.model.pojo.forecast5day.Measurements;
import ru.cyber_eagle_owl.climaticsouls.ui.adapters.DailyWeatherDetailsAdapter;

public class DayWeatherFragment extends Fragment {
    @Nullable
    private ArrayList<Measurements> dailyMeasurementsToShow;
    private DailyWeatherDetailsAdapter dailyWeatherDetailsAdapter;

    private static final String TAG = "Weather/DayWeatherFragment";
    private static final String TAG_FOR_MEASUREMENTS_SAVING = "dailyMeasurementsToShow";

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
            outState.putParcelableArrayList(TAG_FOR_MEASUREMENTS_SAVING, dailyMeasurementsToShow);
    }

    public static DayWeatherFragment newInstance() {
        DayWeatherFragment dayWeatherFragment = new DayWeatherFragment();
        return dayWeatherFragment;
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_day_weather, container, false);

        if (savedInstanceState != null) {
            dailyMeasurementsToShow = savedInstanceState.getParcelableArrayList(TAG_FOR_MEASUREMENTS_SAVING);
        }

        final RecyclerView dayWeatherDetailsRecyclerView = view.findViewById(R.id.dayWeatherDetailsRV);
        dayWeatherDetailsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        dailyWeatherDetailsAdapter = new DailyWeatherDetailsAdapter();
        if (dailyMeasurementsToShow != null) {
            dailyWeatherDetailsAdapter.setDailyMeasurementsToShow(dailyMeasurementsToShow);
        }
        dayWeatherDetailsRecyclerView.setAdapter(dailyWeatherDetailsAdapter);
        dailyWeatherDetailsAdapter.notifyDataSetChanged();
        return view;
    }

    public void setDailyMeasurementsToShow(final ArrayList<Measurements> dailyMeasurementsToShow) {
        this.dailyMeasurementsToShow = dailyMeasurementsToShow;
    }
}
