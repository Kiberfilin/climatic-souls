package ru.cyber_eagle_owl.climaticsouls.ui.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import ru.cyber_eagle_owl.climaticsouls.R;
import ru.cyber_eagle_owl.climaticsouls.model.pojo.forecast5day.Measurements;

public class DailyWeatherDetailsAdapter extends RecyclerView.Adapter<DailyWeatherDetailsAdapter.DailyWeatherDetailsViewHolder> {

    private static final String TAG = "Weather/DailyWeatherDetailsAdapter";
    private ArrayList<Measurements> dailyMeasurementsToShow;
    private Context context;

    private int expandedPosition = -1;

    private RecyclerView recyclerView;

    @NonNull
    @Override
    // В этом методе мы создаем новую ячейку
    public DailyWeatherDetailsViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        context = parent.getContext();
        recyclerView = (RecyclerView) parent;
        View view = LayoutInflater.from(context).inflate(R.layout.card_day_weather_details, parent, false);
        return new DailyWeatherDetailsViewHolder(view);
    }

    @SuppressLint("LongLogTag")
    @Override
    // В этом методе мы привязываем данные к ячейке
    public void onBindViewHolder(@NonNull final DailyWeatherDetailsViewHolder holder, final int position) {
        final boolean isExpanded = position == expandedPosition;
        holder.details.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        holder.moreLessButton.setText(isExpanded ? R.string.day_weather_details_card_button_collapse :
                R.string.day_weather_details_card_button_expand);
        holder.itemView.setActivated(isExpanded);
        holder.moreLessButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                expandedPosition = isExpanded ? -1 : position;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    TransitionManager.beginDelayedTransition(recyclerView);
                }
                notifyDataSetChanged();
            }
        });
        Measurements tmpDailyMeasurements = dailyMeasurementsToShow.get(position);
        if (tmpDailyMeasurements != null) {
            holder.dateOfMeasurementTV.setText(context.getString(R.string.date_and_time,
                    holder.simpleDateFormat.format(new Date(
                    tmpDailyMeasurements.getForecastDataUnixStyle() * 1000))));
            holder.temperatureTV.setText(context.getString(
                    R.string.daily_temperature,
                    tmpDailyMeasurements.getMain().getTempMin(),
                    tmpDailyMeasurements.getMain().getTempMax()));
            holder.pressureTV.setText(context.getString(
                    R.string.pressure,
                    tmpDailyMeasurements.getMain().getPressure()
            ));
            holder.seaLevelPressureTV.setText(context.getString(
                    R.string.sea_level_pressure,
                    tmpDailyMeasurements.getMain().getSeaLevelPressure()
            ));
            holder.grndLevelPressureTV.setText(context.getString(
                    R.string.grnd_level_pressure,
                    tmpDailyMeasurements.getMain().getGrndLevelPressure()
            ));
            holder.humidityPercentageTV.setText(context.getString(
                    R.string.humidity,
                    tmpDailyMeasurements.getMain().getHumidityPercentage(), "%"
            ));
            holder.weatherDescriptionTV.setText(context.getString(
                    R.string.weather_description,
                    tmpDailyMeasurements.getWeather().get(0).getGroupOfWeatherParameters(),
                    tmpDailyMeasurements.getWeather().get(0).getDescription()
            ));
            holder.cloudsPercentageTV.setText(context.getString(
                    R.string.clouds,
                    tmpDailyMeasurements.getClouds().getCloudsPercentage(), "%"
            ));
            holder.windTV.setText(context.getString(
                    R.string.wind,
                    tmpDailyMeasurements.getWind().getSpeed(),
                    tmpDailyMeasurements.getWind().getDeg()
            ));

            boolean isRain = tmpDailyMeasurements.getRain() != null;
            boolean isSnow = tmpDailyMeasurements.getSnow() != null;
            Log.i(TAG, "isRain == " + isRain + ", and isSnow == " + isSnow);

            if (isRain && tmpDailyMeasurements.getRain().getVolumeForThreeHours() != null) {
                if (isSnow && tmpDailyMeasurements.getSnow().getVolumeForThreeHours() != null) {
                    // и дождь и снег
                    holder.precipitationTV.setText(context.getString(
                            R.string.precipitation_rain_and_snow,
                            tmpDailyMeasurements.getRain().getVolumeForThreeHours(),
                            tmpDailyMeasurements.getSnow().getVolumeForThreeHours()
                    ));
                } else {
                    // только дождь, снега нет
                    holder.precipitationTV.setText(context.getString(
                            R.string.precipitation_rain,
                            tmpDailyMeasurements.getRain().getVolumeForThreeHours()
                    ));
                }
            } else {
                if (isSnow && tmpDailyMeasurements.getSnow().getVolumeForThreeHours() != null) {
                    // только снег, дождя нет
                    holder.precipitationTV.setText(context.getString(
                            R.string.precipitation_snow,
                            tmpDailyMeasurements.getSnow().getVolumeForThreeHours()
                    ));
                } else {
                    // нет ни дождя ни снега
                    holder.precipitationTV.setText(R.string.no_precipitation);
                }
            }

        } else {
            holder.dateOfMeasurementTV.setText(R.string.title_updating_weather);
            holder.temperatureTV.setText(R.string.title_updating_weather);
            holder.pressureTV.setText(R.string.title_updating_weather);
            holder.seaLevelPressureTV.setText(R.string.title_updating_weather);
            holder.grndLevelPressureTV.setText(R.string.title_updating_weather);
            holder.humidityPercentageTV.setText(R.string.title_updating_weather);
            holder.weatherDescriptionTV.setText(R.string.title_updating_weather);
            holder.cloudsPercentageTV.setText(R.string.title_updating_weather);
            holder.windTV.setText(R.string.title_updating_weather);
            holder.precipitationTV.setText(R.string.title_updating_weather);
        }
    }

    @Override
    // В этом методе мы возвращаем количество элементов списка
    public int getItemCount() {
        return dailyMeasurementsToShow.size();
    }

    public void setDailyMeasurementsToShow(final ArrayList<Measurements> dailyMeasurementsToShow) {
        Collections.sort(dailyMeasurementsToShow, new DailyMeasurementsComparator());
        this.dailyMeasurementsToShow = dailyMeasurementsToShow;
    }

    public class DailyWeatherDetailsViewHolder extends RecyclerView.ViewHolder {
        public final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm dd.MM.yyyy", Locale.getDefault());
        private TextView dateOfMeasurementTV;
        private TextView temperatureTV;
        private TextView pressureTV;
        private TextView seaLevelPressureTV;
        private TextView grndLevelPressureTV;
        private TextView humidityPercentageTV;
        private TextView weatherDescriptionTV;
        private TextView cloudsPercentageTV;
        private TextView windTV;
        private TextView precipitationTV;
        private LinearLayout details;
        private TextView moreLessButton;

        public DailyWeatherDetailsViewHolder(final View itemView) {
            super(itemView);
            details = itemView.findViewById(R.id.itemsContainer);
            moreLessButton = itemView.findViewById(R.id.button);
            dateOfMeasurementTV = itemView.findViewById(R.id.dateOfMeasurementTV);
            temperatureTV = itemView.findViewById(R.id.temperatureTV);
            pressureTV = itemView.findViewById(R.id.pressureTV);
            seaLevelPressureTV = itemView.findViewById(R.id.seaLevelPressureTV);
            grndLevelPressureTV = itemView.findViewById(R.id.grndLevelPressureTV);
            humidityPercentageTV = itemView.findViewById(R.id.humidityPercentageTV);
            weatherDescriptionTV = itemView.findViewById(R.id.weatherDescriptionTV);
            cloudsPercentageTV = itemView.findViewById(R.id.cloudsPercentageTV);
            windTV = itemView.findViewById(R.id.windTV);
            precipitationTV = itemView.findViewById(R.id.precipitationTV);
        }
    }

    public class DailyMeasurementsComparator implements Comparator<Measurements> {
        @Override
        public int compare(final Measurements o1, final Measurements o2) {
            return o1.getForecastDataUnixStyle().compareTo(o2.getForecastDataUnixStyle());
        }
    }

}
