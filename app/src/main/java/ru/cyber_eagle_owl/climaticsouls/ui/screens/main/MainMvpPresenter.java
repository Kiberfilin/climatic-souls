package ru.cyber_eagle_owl.climaticsouls.ui.screens.main;

import android.support.annotation.Nullable;

import ru.cyber_eagle_owl.climaticsouls.model.DataManager;
import ru.cyber_eagle_owl.climaticsouls.ui.screens.base.MvpPresenter;

public interface MainMvpPresenter<V extends MainMvpView> extends MvpPresenter<V> {

    Long getLastUpdate();

    void saveForecastToDb(String typeOfForecast, String forecastJson);

    void prepareDb(@Nullable DataManager.OnFiveDayForecastReadyListener listener);

}
