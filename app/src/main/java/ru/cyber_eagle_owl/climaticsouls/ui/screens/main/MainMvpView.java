package ru.cyber_eagle_owl.climaticsouls.ui.screens.main;

import android.app.LoaderManager;
import android.location.LocationManager;

import ru.cyber_eagle_owl.climaticsouls.ui.screens.base.MvpView;

public interface MainMvpView extends MvpView {

    void startWeatherService();

    void checkAndRequestGeoPermission();

    LoaderManager getLoaderManagerFromMainMvpView();

    LocationManager getLocationManagerFromMainMvpView();

    void showToastMsg(String textMsg);
}
