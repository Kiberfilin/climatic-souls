package ru.cyber_eagle_owl.climaticsouls.loaders;

import android.annotation.SuppressLint;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import ru.cyber_eagle_owl.climaticsouls.db.DbHelper;
import ru.cyber_eagle_owl.climaticsouls.db.provider.ForecastContract;

public class ForecastDeleteLoader extends AsyncTaskLoader<Boolean>{

    //private Uri uri =  null;
    private DbHelper dbHelper;
    private String selection;
    private String[] selectionArgs;

    private static final String TAG = "Weather/ForecastDeleteLoader";

    public static final String KEY_SELECTION_FOR_OLD_FORECAST_DELETING = "selection_for_old_forecast_deleting";
    public static final String KEY_SELECTION_ARGS_FOR_OLD_FORECAST_DELETING = "selection_args_for_old_forecast_deleting";

    @SuppressLint("LongLogTag")
    public ForecastDeleteLoader(final Context context, final DbHelper dbHelper, Bundle bundle) {
        super(context);
        this.dbHelper = dbHelper;
        this.selection = bundle.getString(KEY_SELECTION_FOR_OLD_FORECAST_DELETING);
        Log.i(TAG, "Получили следующий selection: " + String.valueOf(selection));
        this.selectionArgs = bundle.getStringArray(KEY_SELECTION_ARGS_FOR_OLD_FORECAST_DELETING);
        Log.i(TAG, "Получили следующие selectionArgs: " + String.valueOf(selectionArgs));
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @SuppressLint("LongLogTag")
    @Override
    public Boolean loadInBackground() {
            Log.i(TAG, "Запустили loadInBackground(). Сейчас удаляется/ются старый/е прогноз/ы, id которого/ых: " + selectionArgs.toString());
            return dbHelper.delete(ForecastContract.Forecasts.URI,
                    selection,
                    selectionArgs) != 0;
    }
}
