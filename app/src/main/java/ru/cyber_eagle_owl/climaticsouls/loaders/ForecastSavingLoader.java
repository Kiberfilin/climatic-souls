package ru.cyber_eagle_owl.climaticsouls.loaders;

import android.content.AsyncTaskLoader;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import ru.cyber_eagle_owl.climaticsouls.db.DbHelper;

public class ForecastSavingLoader extends AsyncTaskLoader<Boolean> {

    private DbHelper dbHelper;
    private ContentValues contentValues;
    private Uri uri;

    private static final String TAG = "Weather/" + ForecastSavingLoader.class.getSimpleName();

    public static final String KEY_CONTENT_VALUES = "content_values";
    public static final String KEY_URI = "uri";

    public ForecastSavingLoader(final Context context, final DbHelper dbHelper, Bundle bundle) {
        super(context);
        this.dbHelper = dbHelper;
        this.contentValues = bundle.getParcelable(KEY_CONTENT_VALUES);
        this.uri = bundle.getParcelable(KEY_URI);
    }

    @Override
    public Boolean loadInBackground() {
        return dbHelper.insert(uri, contentValues) != null;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }
}
