package ru.cyber_eagle_owl.climaticsouls.broadcastreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import ru.cyber_eagle_owl.climaticsouls.services.WeatherService;

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            context.startService(new Intent(context, WeatherService.class));
        }
    }

}
