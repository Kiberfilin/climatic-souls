package ru.cyber_eagle_owl.climaticsouls;

import com.google.gson.Gson;

import junit.framework.Assert;

import org.junit.Test;

import java.io.IOException;

import ru.cyber_eagle_owl.climaticsouls.model.api.ApiHelper;
import ru.cyber_eagle_owl.climaticsouls.model.pojo.forecast5day.FiveDayForecast;

public class ApiTest {

    private ApiHelper mApiHelper = new ApiHelper();

    @Test
    public void testCurrentWeather() throws IOException {
        FiveDayForecast fiveDayForecast = mApiHelper.getCallFor5DayForecast(56.8604, 37.3599).execute().body();

        Assert.assertNotNull(fiveDayForecast);
        Gson gson = new Gson();
        String fDFToString = gson.toJson(fiveDayForecast);

        System.out.println(fDFToString);

        FiveDayForecast mFiveDF = gson.fromJson(fDFToString, FiveDayForecast.class);

        System.out.println(mFiveDF.getMeasurementsList().get(32).getWeather().get(0).getDescription());
        /*
        Assert.assertNotNull(cityName);
        System.out.println(cityName.toString());
        Assert.assertTrue(cityName.length() > 0);

        ForecastMain main = currentWeather.getMain();
        Assert.assertNotNull(main);
        Assert.assertNotNull(main.getTemp());
        Assert.assertNotNull(main.getMaxTemp());
        Assert.assertNotNull(main.getMinTemp());
        Assert.assertNotNull(main.getHumidity());
        System.out.println(main.getHumidity());
        Assert.assertNotNull(main.getPressure());

        ForecastWind wind = currentWeather.getWind();
        Assert.assertNotNull(wind);
        Assert.assertNotNull(wind.getDegree());
        System.out.println(wind.getDegree());
        Assert.assertNotNull(wind.getSpeed());
        System.out.println(wind.getSpeed());
        */
    }
}
